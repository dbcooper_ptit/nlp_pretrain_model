1. Preprocess the data, there is a corpus.txt file under corpus, one line represents an article, and then execute pro_data.py to get the pro_data.txt file, which is equivalent to
    Separate the articles by sentence, and then separate each article with a blank line
2. Here we use bert-base configuration config and vocabulary vocab.txt
3. Execute get_train_data.py to get the training data, which includes the token to id and mask the sentence
4. python run_pretrain.py starts pretraining